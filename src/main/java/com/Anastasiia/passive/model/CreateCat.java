package com.Anastasiia.passive.model;

import java.io.*;
import java.util.ArrayList;
import java.util.Scanner;
/**
 * @author Anastasiia Harmash
 * @version 19 nov 2019
 */
public class CreateCat implements AutoCloseable {


    public static ArrayList<Kitten> kittens = new ArrayList<Kitten>();

    public static void CreateCat() {


        String newName = "";
        int newAge = -1;
        int newHarmfulnessLevel = -1;
        String newGender = "";

        while (newName.equals("")) {
            try {
                newName = readKittenName();
            } catch (WrongCreateKittenException e) {
                System.out.println(e.getMessage());
            }
        }

        while ((newAge < 0) || (newAge > 26)) {
            try {
                newAge = readKittenAge();
            } catch (WrongCreateKittenException e) {
                System.out.println(e.getMessage());
            }
        }

        while ((newHarmfulnessLevel < 0) || (newHarmfulnessLevel > 13)) {
            try {
                newHarmfulnessLevel = readKittenHarmfulnessLevel();
            } catch (WrongCreateKittenException e) {
                System.out.println(e.getMessage());
            }
        }

        while ((newGender.equals(""))) {
            try {
                newGender = readKittenGender();
            } catch (WrongCreateKittenException e) {
                System.out.println(e.getMessage());
            }
        }
        Kitten kitten = new Kitten(newName, newAge, newHarmfulnessLevel, newGender);
        kittens.add(kitten);

        try (FileWriter catsFile = new FileWriter("resources/Cats.txt", true)) {
            catsFile.append(String.valueOf(kitten) + '\r' + '\n');
            catsFile.close();

        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    public static void showKittens() {

        try {
            FileReader fr = new FileReader("resources/Cats.txt");
            Scanner scan = new Scanner(fr);

            int i = 1;
            while (scan.hasNextLine()) {
                System.out.println(i + ") " + scan.nextLine());
                i++;
            }


        } catch (IOException e) {
            e.printStackTrace();

        }
        //for (int i = 0; i < kittens.size(); i++) {
        //System.out.println(kittens.get(i));
        //}
    }


    public static String readKittenName() throws WrongCreateKittenException {
        Scanner k = new Scanner(System.in);
        System.out.println("Enter cat name: ");
        String newName;

        newName = k.nextLine();
        if (newName.equals("")) {
            throw new WrongCreateKittenException("Please, write cat name!");
        }
        return newName;
    }

    public static int readKittenAge() throws WrongCreateKittenException {
        Scanner in = new Scanner(System.in);
        System.out.println("Enter cat age (0-25): ");
        int newAge;
        newAge = in.nextInt();

        if (newAge < 0) {
            throw new WrongCreateKittenException("Age of cat can't be < 0!");
        } else {
            if (newAge > 25) {
                throw new WrongCreateKittenException("Sorry, but age of cat can't be > 25");
            }
        }
        return newAge;
    }

    public static int readKittenHarmfulnessLevel() throws WrongCreateKittenException {
        Scanner in = new Scanner(System.in);
        System.out.println("Enter cat harmfulness level (0-12): ");
        int newHarmfulnessLevel;
        newHarmfulnessLevel = in.nextInt();

        if (newHarmfulnessLevel < 0) {
            throw new WrongCreateKittenException("Harmfulness level of cat can't be < 0!");
        } else {
            if (newHarmfulnessLevel > 12) {
                throw new WrongCreateKittenException("Harmfulness level of cat can't be > 12!");
            }
        }
        return newHarmfulnessLevel;
    }

    public static String readKittenGender() throws WrongCreateKittenException {
        Scanner in = new Scanner(System.in);
        System.out.println("Choice cat gender (male = 1; female = 2): ");
        int choiceGender;
        String newGender;
        choiceGender = in.nextInt();

        if (choiceGender == 1) {
            newGender = "male";
        } else {
            if (choiceGender == 2) {
                newGender = "female";
            } else {
                throw new WrongCreateKittenException("Wrong gender!");
            }
        }
        return newGender;
    }

    public void close() throws Exception {}
}
