package com.Anastasiia.passive.model;
/**
 * @author Anastasiia Harmash
 * @version 19 nov 2019
 */
public interface MotherCat {

    void Sound();

    void Move();

}
