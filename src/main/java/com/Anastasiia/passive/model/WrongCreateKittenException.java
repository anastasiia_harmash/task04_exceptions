package com.Anastasiia.passive.model;
/**
 * @author Anastasiia Harmash
 * @version 19 nov 2019
 */
public class WrongCreateKittenException extends Exception {

    public WrongCreateKittenException(String message) {
        super(message);
    }
}
