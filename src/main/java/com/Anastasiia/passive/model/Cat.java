package com.Anastasiia.passive.model;
/**
 * @author Anastasiia Harmash
 * @version 19 nov 2019
 */
public class Cat {

    public String name;
    public int age;
    public int harmfulnessLevel;
    public String gender;

    public Cat(String name, int age, int harmfulnessLevel, String gender) {
        this.name = name;
        this.age = age;
        this.harmfulnessLevel = harmfulnessLevel;
        this.gender = gender;
    }


    @Override
    public String toString() {
        return
                "name = " + name +
                        ", age = " + age +
                        ", harmfulnessLevel = " + harmfulnessLevel +
                        ", gender = " + gender;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public int getHarmfulnessLevel() {
        return harmfulnessLevel;
    }

    public void setHarmfulnessLevel(int harmfulnessLevel) {
        this.harmfulnessLevel = harmfulnessLevel;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }
}
