package com.Anastasiia.passive.model;

import com.Anastasiia.passive.view.View;

/**
 * @author Anastasiia Harmash
 * @version 19 nov 2019
 */
public class Main {

    public static void main(String[] args) {

        View.menu();
    }
}
