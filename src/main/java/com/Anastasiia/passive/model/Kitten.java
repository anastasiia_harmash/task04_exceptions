package com.Anastasiia.passive.model;
/**
 * @author Anastasiia Harmash
 * @version 19 nov 2019
 */
public class Kitten extends Cat implements FatherCat, MotherCat {

    public Kitten(String name, int age, int harmfulnessLevel, String gender) {
        super(name, age, harmfulnessLevel, gender);
    }

    public void Hunt() {
        System.out.println("I'm hunting!");
    }

    public void Play() {

        System.out.println("I'm playing!");

    }

    public void Sound() {

        System.out.println("Meow-meooow-meoooow!");

    }

    public void Move() {

        System.out.println("I am leaving here! Bye!");

    }

    @Override
    public String toString() {
        return
                "name = " + name +
                        ", age = " + age +
                        ", harmfulnessLevel = " + harmfulnessLevel +
                        ", gender = " + gender;
    }

}



