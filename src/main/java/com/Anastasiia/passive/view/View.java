package com.Anastasiia.passive.view;

import com.Anastasiia.passive.controller.Controller;

import java.util.Scanner;

/**
 * @author Anastasiia Harmash
 * @version 19 nov 2019
 */
public class View {

    public static void menu() {

        int menuPoint;
        do {

            System.out.println("1 - Create new cat");
            System.out.println("2 - Show my cats");
            System.out.println("3 - exit");
            System.out.println("Please, make your choice:");

            Scanner k = new Scanner(System.in);
            menuPoint = k.nextInt();

            if (menuPoint == 1) {
                Controller.createCat();

            } else {
                if (menuPoint == 2) {
                    Controller.showKittens();
                }
            }
        } while (menuPoint != 3);

    }
}
