package com.Anastasiia.passive.controller;

import com.Anastasiia.passive.model.CreateCat;
/**
 * @author Anastasiia Harmash
 * @version 19 nov 2019
 */

public class Controller {

    public static void createCat() {
        CreateCat.CreateCat();
    }

    public static void showKittens() {
        CreateCat.showKittens();
    }
}
